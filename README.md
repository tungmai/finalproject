Getting Started
---------------

### Prerequisites


- [Node.js](http://nodejs.org)
- Command Line Tools (some NPM modules must be compiled):
 - **Mac OS X**: [Xcode](https://itunes.apple.com/us/app/xcode/id497799835?mt=12) (or **OS X 10.9 Mavericks**: `xcode-select --install`)
 - **Windows**: [Visual Studio](http://www.visualstudio.com/downloads/download-visual-studio-vs#d-express-windows-8)
 - **Ubuntu**: `sudo apt-get install build-essential`
 - **Fedora**: `sudo yum groupinstall "Development Tools"`
 - **OpenSUSE**: `sudo zypper install --type pattern devel_basis`

### Installation

```bash
# Install global dependencies
npm install -g nodemon 

# Clone the repo (and fetch only the latest commits)
git clone git@gitlab.com:tungmai/finalproject.git
cd finalproject

# Install local dependencies
npm install
bower install

# Start everything up with Gulp
# (builds the assets and starts the app with nodemon)
npm start
```